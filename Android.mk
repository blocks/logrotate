LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:=\
	basenames.c \
	config.c \
	log.c \
	logrotate.c\
	poptparse.c \
	popthelp.c \
	poptconfig.c \
	popt.c \
	findme.c \
	glob-strip.c
	
LOCAL_SRC_INCLUDES:=\
    basenames.h \
    config.h \
    log.h \
    logrotate.h \
    glob.h 
    
LOCAL_C_INCLUDES := \
        external/logrotate/libpopt

LOCAL_CFLAGS:=-O2 -g

LOCAL_MODULE_TAGS := eng

LOCAL_MODULE_PATH := $(TARGET_OUT_OPTIONAL_EXECUTABLES)

LOCAL_MODULE:=logrotate

LOCAL_SHARED_LIBRARIES := \
    libc \
    libstdc++ \
    libcutils \
    libutils
	
# gold in binutils 2.22 will warn about the usage of mktemp
LOCAL_LDFLAGS += -Wl,--no-fatal-warnings

include $(BUILD_EXECUTABLE)
