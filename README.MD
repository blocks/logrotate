logrotate
---------

This tool is a port from Linux logrotate (https://fedorahosted.org/logrotate/) version 3.8.6. Ported to be able to run on Android and use it as normal logrotate in Linux environment. 

NOTE: this is an early release as there are few kinks that need to be ironed out before it's production ready. Credit goes to AOSP source to host most of the header files and structures that can be reused for this project.
